import { connect } from '../Connections/movie_db';

export class Patients {
    id: number;
    nom: string;
    prenom: string;
    adresse: string;
    mail: string;
    date_naissance: string;
    diagnostique: string;
    tel: string;

    constructor(data: any) {
        this.id = data.id;
        this.nom = data.nom;
        this.prenom = data.prenom;
        this.adresse = data.adresse;
        this.mail = data.mail;
        this.date_naissance = data.date_naissance;
        this.diagnostique = data.diagnostique;
        this.tel = data.tel;
    }

}
export class PatientsModel {
    public static async getAll() {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM patients').then((results) => {
                return results;
            });
        });
    }

    public static async getOneById(id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM patients WHERE id = ?', id).then((results) => {
                return results;
            });
        });
    }

    public static async insertPatient(patient: Patients) {
        console.log(patient);
        return connect().then((conn) => {
            return conn.query('INSERT INTO patients ( nom, prenom, adresse, mail, date_naissance, diagnostique, tel) VALUES (?,?,?,?,?,?,?)', [patient.nom, patient.prenom, patient.adresse, patient.mail, patient.date_naissance, patient.diagnostique, patient.tel]).then((results) => {
                return this.getAll();
            });
        });
    }

    public static async deletePatientById(id: any) {
        return connect().then((conn) => {
            return conn.query('DELETE FROM patients WHERE id = ?', id).then((results) => {
                return this.getAll();
            });
        });
    }

    public static async updatePatientById(id: any, patient: Patients) {
        return connect().then((conn) => {
            return conn.query('UPDATE patients SET nom=?, prenom=?, adresse=?, mail=?, date_naissance=?, diagnostique=?, tel=? WHERE id =?',
                [patient.nom, patient.prenom, patient.adresse, patient.mail, patient.date_naissance, patient.diagnostique, patient.tel, id]).then((results) => {
                    return this.getAll();
                });
        });
    }
}