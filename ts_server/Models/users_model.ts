import { connect } from '../Connections/movie_db';

export class Users {
    id: number;
    username: string;
    email: string;
    password: string;
    admin: boolean;


    constructor(data: any) {
        this.id = data.id;
        this.username = data.username;
        this.email = data.email;
        this.password = data.password;
        this.admin = data.admin ? data.admin : 0;

    }

}
export class UsersModel {
    public static async getAll() {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM users').then((results) => {
                return results;
            });
        });
    }

    public static async getOneById(id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM users WHERE id = ?', id).then((results) => {
                return results;
            });
        });
    }

    public static async getOneByName(name: any) {
        return connect().then((conn) => {
            return conn.query('SELECT id, username, email, admin FROM users WHERE username=?', name).then((results) => {
                return results;
            });
        });
    }

    public static async getOneByEmail(email: any) {
        return connect().then((conn) => {
            return conn.query('SELECT id, username, email, admin FROM users WHERE email=?', email).then((results) => {
                return results;
            });
        })
    }


    public static async insertUser(user: Users) {
        console.log(user);
        return connect().then((conn) => {
            return conn.query('INSERT INTO users ( username, email, password) VALUES (?,?,?)', [user.username, user.email, user.password]).then((results) => {
                return this.getAll();
            });
        });
    }

    public static async deleteUserById(id: any) {
        return connect().then((conn) => {
            return conn.query('DELETE FROM users WHERE id = ?', id).then((results) => {
                return this.getAll();
            });
        });
    }

    public static async updateUserById(id: any, user: Users) {
        return connect().then((conn) => {
            return conn.query('UPDATE users SET username=?, email=?, admin=? WHERE id =?',
                [user.username, user.email, user.admin, id]).then((results) => {
                    return this.getOneById(id);
                });
        });
    }

    public static async checkPassword(username: string, password: string): Promise<any> {
        let res = await connect().then((conn) => {
            return conn.query('SELECT id, username, email, password, admin FROM users WHERE username=?', username).then((results) => {
                return results;
            });
        });
        if (res[0].password === password) {
            return { success: true, admin: res[0].admin };
        } return { success: false, admin: false };
    }

    public static async updatePassword(user: Users) {
        return connect().then((conn) => {
            return conn.query('UPDATE users SET password=? WHERE id=?', [user.password, user.id]).then((results) => {
                return this.getOneById(user.id);
            });
        });
    }

}