import { Router } from 'express';
import { UsersController } from '../controllers/users_controller';


export class UsersRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.router.get('/users', UsersController.getAll);
        this.router.get('/users/id/:id', UsersController.getOneById);
        // this.router.get('/name/:name', UsersController.getOneByName);
        // this.router.get('/email/:email', UsersController.getOneByEmail);

        //this.router.post('/users/create', UsersController.createUser);
        this.router.delete('/users/:id', UsersController.deleteUser);
        this.router.put('/users/:id', UsersController.updateUserById);

    }
}