
import { Router } from 'express';
import { UsersController } from '../Controllers/users_controller'

export class UsersCommonRouter {
    public router: Router;
    public authRouter: Router;

    constructor() {
        this.router = Router();

        this.router.get('/users/name/:name', UsersController.getOneByName);

        this.router.get('/users/id/:id', UsersController.getOneById);

        this.router.post('/users/create', UsersController.createUser);

        this.authRouter = Router();

        this.authRouter.put('users/password', UsersController.updateUserPassword);

        this.authRouter.put('/users/:id', UsersController.updateConnectedUser);
    }
}