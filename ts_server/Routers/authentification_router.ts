import { Router, Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { Users, UsersModel } from '../Models/users_model';

export class AuthentificationRouter {

    public router: Router;

    constructor() {
        this.router = Router();
        this.router.post('/', this.getToken);
    }

    public static checkAuthorization(req: any, res: any, next: any) {
        let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization'];

        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, 'my-super-secret-key', function (err: any, decoded: any) {
                if (err) {
                    return res.json({ success: false, message: 'Pas de token d authentification' });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else { return res.status(403).send({ success: false, message: 'Pas de token fournis' }) };
    }


    public async getToken(req: Request, res: Response, next: NextFunction) {

        const pseudo = req.body.username || null;
        const password = req.body.password || null;

        let result = await UsersModel.checkPassword(pseudo, password);
        if (result.success) {
            const token = jwt.sign({ username: req.body.username, admin: result.admin },
                'my-super-secret-key',
                { expiresIn: 600 });
            res.json({ success: true, message: 'logged in', token: token, admin: result.admin });
        } else {
            res.json({ success: false, message: 'mauvais pseudo ou mot de passe' });
        }
    }

    public static checkAdmin(req: any, res: any, next: any) {
        if (req.decoded.admin) {
            next();
        } else {
            return res.status(403).send({
                success: false,
                message: 'vous devez être administrateur'
            });
        }
    }


}