import { Router, Request, Response, NextFunction } from 'express';
import { PatientsModel, Patients } from '../Models/patients_model';

export namespace PatientsController {

    export async function getAll(req: Request, res: Response, next: NextFunction) {

        const results = await PatientsModel.getAll();
        res.json(results);
    }

    export async function getOneById(req: Request, res: Response, next: NextFunction) {

        const results = await PatientsModel.getOneById(req.params.id);
        res.json(results);
    }

    export async function createPatient(req: Request, res: Response, next: NextFunction) {
        try {
            const patient = new Patients(req.body);
            const results = await PatientsModel.insertPatient(patient);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }

    export async function deletePatient(req: Request, res: Response, next: NextFunction) {

        const results = await PatientsModel.deletePatientById(req.params.id);
        res.json(results);
    }

    export async function updatePatientById(req: Request, res: Response, next: NextFunction) {
        try {
            const patient = new Patients(req.body);
            const results = await PatientsModel.updatePatientById(req.params.id, patient);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }
}