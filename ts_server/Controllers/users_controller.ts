import { Router, Request, Response, NextFunction } from 'express';
import { UsersModel, Users } from '../Models/users_model';

export namespace UsersController {

    export async function getAll(req: Request, res: Response, next: NextFunction) {

        const results = await UsersModel.getAll();
        res.json(results);
    }

    export async function getOneById(req: Request, res: Response, next: NextFunction) {

        const results = await UsersModel.getOneById(req.params.id);
        res.json(results);
    }

    export async function getOneByName(req: Request, res: Response, next: NextFunction) {
        const results = await UsersModel.getOneByName(req.params.username);
        res.json(results);
    }

    export async function getOneByEmail(req: Request, res: Response, next: NextFunction) {
        const results = await UsersModel.getOneByEmail(req.params.email);
        res.json(results);
    }


    export async function createUser(req: Request, res: Response, next: NextFunction) {
        try {
            const user = new Users(req.body);
            const results = await UsersModel.insertUser(user);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }

    export async function deleteUser(req: Request, res: Response, next: NextFunction) {

        const results = await UsersModel.deleteUserById(req.params.id);
        res.json(results);
    }

    export async function updateUserById(req: Request, res: Response, next: NextFunction) {
        try {
            const user = new Users(req.body);
            const results = await UsersModel.updateUserById(req.params.id, user);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }

    export async function updateConnectedUser(req: any, res: Response, next: NextFunction) {
        try {
            const result = await UsersModel.getOneById(req.params.id);
            const user = new Users(result[0]);
            const updatedUser = new Users(req.body)

            const username = req.decoded.username;

            if (username === user.username) {
                const results = await UsersModel.updateUserById(req.params.id, updatedUser);
                res.json(results);
            } else {
                res.status(403).send({
                    success: false,
                    message: 'Vous ne pouvez changer cet utilisateur.'
                });
            }
        } catch (err) {
            res.status(500).send(err);
        }
    }

    export async function updateUserPassword(req: any, res: Response, next: NextFunction) {
        try {
            const user = new Users(req.body);
            const username = req.decoded.username;
            if (username === user.username) {
                const results = await UsersModel.updatePassword(user);
                res.json(results);
            } else {
                res.status(403).send({
                    success: false,
                    message: 'Vous ne pouvez changer cet utilisateur.'
                });
            }
        } catch (err) {
            res.status(500).send(err);
        }
    }


}