import * as express from 'express';
import { PatientsRouter } from './Routers/patients_router';
import * as https from 'https';
import * as http from 'http';
import * as cors from 'cors';
import * as fs from 'fs';
import { UsersRouter } from './Routers/users_router';
import { AuthentificationRouter } from './Routers/authentification_router';
import { UsersCommonRouter } from './Routers/users_common_router';

export class Server {
    private app: express.Application;
    private httpsServer: https.Server;
    private httpServer: http.Server;

    constructor() {

        this.app = express();
        this.app.use(cors());

        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));

        this.init_routes();


        let key = fs.readFileSync('certificate/server.key');
        let certif = fs.readFileSync('certificate/server.cert');
        let credentials = { key: key, cert: certif };
        this.httpsServer = https.createServer(credentials, this.app);
        this.httpServer = http.createServer(this.app);
    }

    private init_routes() {
        this.app.use('/api/token', new AuthentificationRouter().router)
        this.app.use('/api', new UsersCommonRouter().router);

        this.app.use(AuthentificationRouter.checkAuthorization);

        this.app.use('/api', new UsersCommonRouter().authRouter);

        this.app.use('/api', new PatientsRouter().router);
        this.app.use(AuthentificationRouter.checkAdmin);
        this.app.use('/api', new UsersRouter().router);


    }

    public start() {
        this.httpsServer.listen(4430);
        this.httpServer.listen(80);

    }
}